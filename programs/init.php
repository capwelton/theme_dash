<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';


/**
 * Called by ovidentia core when upgrading addon.
 *
 * @param string $version_base
 * @param string $version_ini
 * @return boolean
 */
function theme_dash_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    $addon = bab_getAddonInfosInstance('theme_dash');
    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('theme_dash');
        $addon->addEventListener('bab_eventBeforePageCreated', 'theme_dash_addCssAndJs', 'init.php', -10);
        $addon->registerFunctionality('Ovml/Function/ThemeDashGetRegistryValue', 'ovml_configuration.php');
        $addon->registerFunctionality('Ovml/Function/ThemeDashImageBase64', 'ovml_configuration.php');
        $addon->registerFunctionality('Ovml/Function/ThemeDashGetImageUrl', 'ovml_configuration.php');
    } else {
        // We set a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
        // It must also be executed after other addon may have changed the global babSkin variable.
        bab_addEventListener('bab_eventBeforePageCreated', 'theme_dash_addCssAndJs', 'addons/theme_dash/init.php', 'theme_dash', -10);

        $func = new bab_functionalities;
        $func->register('Ovml/Function/ThemeDashGetRegistryValue', dirname(__FILE__).'/ovml_configuration.php');
        $func->register('Ovml/Function/ThemeDashImageBase64', dirname(__FILE__).'/ovml_configuration.php');
        $func->register('Ovml/Function/ThemeDashGetImageUrl', dirname(__FILE__).'/ovml_configuration.php');
    }

    return true;
}



/**
 * Called by ovidentia core when deleting addon.
 */
function theme_dash_onDeleteAddon()
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    if (function_exists('bab_removeAddonEventListeners')) {
        $addon = bab_getAddonInfosInstance('theme_dash');
        bab_removeAddonEventListeners('theme_dash');
        $addon->unregisterFunctionality('Ovml/Function/ThemeDashGetRegistryValue');
        $addon->unregisterFunctionality('Ovml/Function/ThemeDashImageBase64');
        $addon->unregisterFunctionality('Ovml/Function/ThemeDashGetImageUrl');
    } else {
        bab_removeEventListener('bab_eventBeforePageCreated', 'theme_dash_addCssAndJs', 'addons/theme_dash/init.php');
        $func = new bab_functionalities;
        $func->unregister('Ovml/Function/ThemeDashGetRegistryValue');
        $func->unregister('Ovml/Function/ThemeDashImageBase64');
        $func->unregister('Ovml/Function/ThemeDashGetImageUrl');
    }

    return true;
}




/**
 * Adds theme specific css and javascript to the page.
 *
 * Must be executed with a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
 * It must also be executed after other addon may have changed the global babSkin variable.
 */
function theme_dash_addCssAndJs()
{
    if (bab_isAjaxRequest()) {
        return;
    }
    global $babSkin;
    if ('theme_dash' !== $babSkin) {
        return;
    }

    $babBody = bab_getBody();

     $Icons = bab_functionality::get('Icons');
     $Icons->includeCss();

    $jquery = bab_functionality::get('jquery');
    $jquery->includeCore();

    /* @var $Less Func_Less */
//     if ($Less = @bab_functionality::get('less')) {
//         $Less->setCompiledCssPath(theme_dash_getCompiledCssPath());
//         $Less->setCompiledCssBaseUrl(theme_dash_getCompiledCssBaseUrl());

//         $variables = theme_dash_getLessVariables();

//         $rootPath = realpath('.');
//         $addon = bab_getAddonInfosInstance('theme_dash');
//         $stylesPath = $rootPath . '/' . $addon->getThemePath() . 'styles/';

//         try {
//             $stylesheet = $Less->getCssUrl($stylesPath.'style.less', $variables, false);
//             $babBody->addStyleSheet($stylesheet);
//         } catch (Exception $e) {
//             bab_debug($e->getMessage());
//         }
//     }

}
