;<?php /*

[general]
name				="theme_dash"
version				="1.1.1"
addon_type			="THEME"
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
delete				=1
ov_version			="8.4.94"
php_version			="5.1.3"
mysql_version		="4.0"
author				="cantico"
addon_access_control = "0"
configuration_page  ="configuration"
icon                ="icon.png"
tags                ="theme,crm,intranet"

[addons]
jquery				="1.4.4.1"
widgets				="1.1.14"
LibOrm				="0.7.6"
LibFileManagement 	="0.2.14"
;LibLess             ="0.3.8.0"
sitemap_editor      ="0.5.6"
portlets            ="0.8"

[functionalities]
jquery				="Available"
Thumbnailer			="Available"

;*/ ?>
