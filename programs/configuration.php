<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


/**
 * Returns the theme's images folder path.
 *
 * @return bab_Path
 */
function theme_dash_getImagePath($directory)
{
    $addon = bab_getAddonInfosInstance('theme_dash');

    $ovidentiapath = realpath('.');

    $uploadPath = new bab_Path($ovidentiapath, 'images',  $addon->getRelativePath(), $directory);
    if (!$uploadPath->isDir()) {
        $uploadPath->createDir();
    }

    return $uploadPath;
}



/**
 * Displays a form to edit the theme configuration.
 */
function theme_dash_editConfiguration()
{
    bab_Functionality::includefile('Icons');
    $W = bab_Widgets();
    $page = $W->BabPage();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_dash/global');

    $form = $W->Form();

    $form->addClass(Func_Icons::ICON_LEFT_16);

    $siteSitemap = bab_Sitemap::getSiteSitemap();

    $nodeSection = $W->Section(
        theme_dash_translate('Navigation nodes'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Top navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('topNavigationNode')
                    ->setValue($registry->getValue('topNavigationNode'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Bottom navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('bottomNavigationNode')
                    ->setValue($registry->getValue('bottomNavigationNode'))
            )
        )
    );
    $faviconImagePicker = $W->ImagePicker();
    $faviconImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(32, 32)
        ->setTitle(theme_dash_translate('favicon'))
        ->setName('faviconImage');

    $bannerImagePicker = $W->ImagePicker();
    $bannerImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_dash_translate('Banner image'))
        ->setName('bannerImage');

    $logoImagePicker = $W->ImagePicker();
    $logoImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_dash_translate('Logo image'))
        ->setName('logoImage');

    $sidebarBackgroundImagePicker = $W->ImagePicker();
    $sidebarBackgroundImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_dash_translate('Sidebar image'))
        ->setName('sidebarImage');

    $loginImagePicker = $W->ImagePicker();
    $loginImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_dash_translate('Login image'))
        ->setName('loginImage');

    $imagesSection = $W->Section(
        theme_dash_translate('Images'),
        $W->FlowItems(
            $faviconImagePicker,
            $bannerImagePicker,
            $logoImagePicker,
            $sidebarBackgroundImagePicker,
            $loginImagePicker
        )->setHorizontalSpacing(2, 'em')
    );

    $faviconImageFolder = theme_dash_getImagePath('favicon');
    $faviconImagePicker->importPath($faviconImageFolder, 'UTF-8');

    $logoImageFolder = theme_dash_getImagePath('logo');
    $logoImagePicker->importPath($logoImageFolder, 'UTF-8');

    $bannerImageFolder = theme_dash_getImagePath('banner');
    $bannerImagePicker->importPath($bannerImageFolder, 'UTF-8');

    $sidebarBackgroundImageFolder = theme_dash_getImagePath('sidebar');
    $sidebarBackgroundImagePicker->importPath($sidebarBackgroundImageFolder, 'UTF-8');

    $loginImageFolder = theme_dash_getImagePath('login');
    $loginImagePicker->importPath($loginImageFolder, 'UTF-8');

    $colorSection = $W->Section(
        theme_dash_translate('Colors'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Header background'),
                $W->ColorPicker()
                    ->setName('headerBackgroundColor')
                    ->setValue(substr($registry->getValue('headerBackgroundColor'), 1))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Main color'),
                $W->ColorPicker()
                    ->setName('mainColor')
                    ->setValue(substr($registry->getValue('mainColor'), 1))
            )
        )
    );

    $advancedSection = $W->Section(
        theme_dash_translate('Advanced'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Global css'),
                $W->TextEdit()
                    ->setLines(10)
                    ->addClass('widget-100pc', 'widget-autoresize')
                    ->setSizePolicy('widget-100pc')
                    ->setName('globalCss')
                    ->setValue($registry->getValue('globalCss'))
            )
        )
    );

    $otherSection = $W->Section(
        theme_dash_translate('Other'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Max width'),
                $W->LineEdit()
                    ->setName('maxWidth')
                    ->setValue($registry->getValue('maxWidth'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Header height'),
                $W->LineEdit()
                    ->setName('headerHeight')
                    ->setValue($registry->getValue('headerHeight'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Header text'),
                $W->LineEdit()
                    ->setName('headerText')
                    ->setValue($registry->getValue('headerText'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Sub-header text'),
                $W->LineEdit()
                    ->setName('subHeaderText')
                    ->setValue($registry->getValue('subHeaderText'))
            )
        )
    );
    $displayMoreBgColor = $W->VBoxItems(
        theme_dash_LabelledWidget(
            theme_dash_translate('Main font color'),
            $W->ColorPicker()
                ->setName('customSidebar_mainFontColor')
                ->setValue(substr($registry->getValue('customSidebar_mainFontColor'), 1))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Secondary font color'),
            $W->ColorPicker()
                ->setName('customSidebar_secondFontColor')
                ->setValue(substr($registry->getValue('customSidebar_secondFontColor'), 1))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Main background color'),
            $W->ColorPicker()
            ->setName('customSidebar_firstBackground')
            ->setValue(substr($registry->getValue('customSidebar_firstBackground'), 1))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Second background color'),
            $W->ColorPicker()
            ->setName('customSidebar_secondBackground')
            ->setValue(substr($registry->getValue('customSidebar_secondBackground'), 1))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Third background color'),
            $W->ColorPicker()
            ->setName('customSidebar_thirdBackground')
            ->setValue(substr($registry->getValue('customSidebar_thirdBackground'), 1))
        )
    )->addAttribute('style', 'padding-left:20px;');
    $sidebarSection = $W->Section(
        theme_dash_translate('Sidebar'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Theme'),
                $W->Select()
                    ->addOption('abyss', 'Abyss')
                    ->addOption('grass', 'Grass')
                    ->addOption('customSidebar', theme_dash_translate('Custom'))
                    ->setName('sidebarTheme')
                    ->setValue($registry->getValue('sidebarTheme'))
                    ->setAssociatedDisplayable($displayMoreBgColor, array('customSidebar'))
            ),
            $displayMoreBgColor,
            theme_dash_LabelledWidget(
                theme_dash_translate('Fixed'),
                $W->Select()
                    ->addOption('sidebar-scroll-fixed', theme_dash_translate('Yes'))
                    ->addOption('sidebar-scroll-default', theme_dash_translate('No'))
                    ->setName('sidebarFixed')
                    ->setValue($registry->getValue('sidebarFixed'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Display search bar'),
                $W->CheckBox()
                    ->setName('sidebarSearchBar')
                    ->setValue($registry->getValue('sidebarSearchBar'))
            ),
            theme_dash_LabelledWidget(
                theme_dash_translate('Opened by default'),
               $W->CheckBox()
                    ->setName('sidebarOpenedDefault')
                    ->setValue($registry->getValue('sidebarOpenedDefault'))
            )
        )
    );

    $displayMoreTitleFonts = $W->VBoxItems(
        theme_dash_LabelledWidget(
            theme_dash_translate('Link from Google Fonts'),
            $W->LineEdit()
                ->setName('customTitleFontUrl')
                ->setValue($registry->getValue('customTitleFontUrl'))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Font name (case sensitive)'),
            $W->LineEdit()
                ->setName('customTitleFontName')
                ->setValue($registry->getValue('customTitleFontName'))
        )
    )->addAttribute('style', 'padding-left:20px;');
    $displayMoreGlobalFonts = $W->VBoxItems(
       theme_dash_LabelledWidget(
            theme_dash_translate('Link from Google Fonts'),
            $W->LineEdit()
                ->setName('customGlobalFontUrl')
                ->setValue($registry->getValue('customGlobalFontUrl'))
        ),
        theme_dash_LabelledWidget(
            theme_dash_translate('Font name (case sensitive)'),
            $W->LineEdit()
                ->setName('customGlobalFontName')
                ->setValue($registry->getValue('customGlobalFontName'))
        )
    )->addAttribute('style', 'padding-left:20px;');
    $fontSection = $W->Section(
        theme_dash_translate('Fonts'),
        $W->VBoxItems(
            theme_dash_LabelledWidget(
                theme_dash_translate('Titles font'),
                $W->Select()
                    ->addOption('Arial', 'Arial')
                    ->addOption('Droid Sans', 'Droid Sans')
                    ->addOption('Open Sans', 'Open Sans')
                    ->addOption('theme_dash_CustomTitleFont', theme_dash_translate('Custom'))
                    ->setName('theme_dash_title_font')
                    ->setValue($registry->getValue('theme_dash_title_font'))
                    ->setAssociatedDisplayable($displayMoreTitleFonts, array('theme_dash_CustomTitleFont'))
            ),
            $displayMoreTitleFonts,
            theme_dash_LabelledWidget(
                theme_dash_translate('Global font'),
                $W->Select()
                ->addOption('Arial', 'Arial')
                ->addOption('Droid Sans', 'Droid Sans')
                ->addOption('Open Sans', 'Open Sans')
                ->addOption('theme_dash_CustomGlobalFont', theme_dash_translate('Custom'))
                ->setName('theme_dash_global_font')
                ->setValue($registry->getValue('theme_dash_global_font'))
                ->setAssociatedDisplayable($displayMoreGlobalFonts, array('theme_dash_CustomGlobalFont'))
                ),
            $displayMoreGlobalFonts
        )
    );

    $form->addItem(
        $W->VBoxItems(
            $W->Frame(null,
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->VBoxItems(
                            $colorSection,
                            $nodeSection,
                            $otherSection,
                            $fontSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-67pc'),
                        $W->VBoxItems(
                            $imagesSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-33pc')
                    )->setVerticalAlign('top'),
                    $sidebarSection->setFoldable(true),
                    $advancedSection->setFoldable(true)
                )->setVerticalSpacing(2, 'em')
            )->setName('configuration'),

            $W->FlowItems(
                $W->SubmitButton()
                    ->setName('idx[save]')
                    ->setLabel(theme_dash_translate('Save configuration')),
                $W->SubmitButton()
                    ->setName('idx[cancel]')
                    ->setLabel(theme_dash_translate('Cancel'))
            )->setHorizontalSpacing(1, 'em')

        )->setVerticalSpacing(3, 'em')
    );

    $form->addClass('widget-bordered');

    $form->setHiddenValue('tg', bab_rp('tg'));

    $page->setTitle(theme_dash_translate('Theme configuration'));
    $page->addItem($form);

    $page->displayHtml();
}




function theme_dash_getImageUrl($imageName, $baseFolder = '')
{
    $W = bab_Widgets();

    $imagesPath = theme_dash_getImagePath($imageName);
    try {
        $imagesPath->deleteDir();
    } catch (bab_FolderAccessRightsException $e) { }
    $imagesPath->createDir();

    $imagePicker = $W->ImagePicker()
        ->setName($imageName . 'Image');
    if ($files = $imagePicker->getTemporaryFiles()) {
        foreach ($files as $file) {
            rename($file->getFilePath()->toString(), $imagesPath->toString() . '/' . $file->getFileName());
        }
    }

    $imageUrl = "''";
    foreach ($imagesPath as $imagePath) {
        $imageFilename = basename($imagePath->toString());
        $imageFilename = str_replace("'", "%27", $imageFilename);
        $imageUrl = "'" . $baseFolder . $imageName . '/' .  $imageFilename . "'";
        break;
    }

    return $imageUrl;
}

/**
 * Saves the posted configuration.
 *
 * @param array $configuration
 */
function theme_dash_saveConfiguration($configuration)
{

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_dash/global');

    // **** NAVIGATION

    if (isset($configuration['topNavigationNode']) && is_string($configuration['topNavigationNode'])) {
        $registry->setKeyValue('topNavigationNode', $configuration['topNavigationNode']);
    }
    if (isset($configuration['bottomNavigationNode']) && is_string($configuration['bottomNavigationNode'])) {
        $registry->setKeyValue('bottomNavigationNode', $configuration['bottomNavigationNode']);
    }

    // **** HEADER
    if (isset($configuration['maxWidth']) && is_string($configuration['maxWidth'])) {
        if (!preg_match('/[0-9\.](px|em|\%|pt|cm|mm|rem)/', $configuration['maxWidth'])) {
            $configuration['maxWidth'] = 0;
        }
        $registry->setKeyValue('maxWidth', $configuration['maxWidth']);
    }
    if (isset($configuration['headerHeight']) && is_string($configuration['headerHeight'])) {
        if (!preg_match('/[0-9\.](px|em|\%|pt|cm|mm|rem)/', $configuration['headerHeight'])) {
            $configuration['headerHeight'] = 0;
        }
        $registry->setKeyValue('headerHeight', $configuration['headerHeight']);
    }
    if (isset($configuration['headerText']) && is_string($configuration['headerText'])) {
        $registry->setKeyValue('headerText', $configuration['headerText']);
    }
    if (isset($configuration['subHeaderText']) && is_string($configuration['subHeaderText'])) {
        $registry->setKeyValue('subHeaderText', $configuration['subHeaderText']);
    }

    // **** GLOBAL CSS

    if (isset($configuration['globalCss']) && is_string($configuration['globalCss'])) {
        $value = $configuration['globalCss'];
        $registry->setKeyValue('globalCss', $value);
    }

    // **** FONTS

    if (isset($configuration['theme_dash_title_font']) && is_string($configuration['theme_dash_title_font'])) {
        $registry->setKeyValue('theme_dash_title_font', $configuration['theme_dash_title_font']);
        // Title font is custom
        if($configuration['theme_dash_title_font'] == 'theme_dash_CustomTitleFont') {
            $registry->setKeyValue('theme_dash_UsedTitleFontUrl', $configuration['customTitleFontUrl']);
            $registry->setKeyValue('theme_dash_UsedTitleFontName', "'".$configuration['customTitleFontName']."'");
        }
        // Title font is predefined
        else{
            $registry->setKeyValue('theme_dash_UsedTitleFontUrl', "''");
            $registry->setKeyValue('theme_dash_UsedTitleFontName', "'".$configuration['theme_dash_title_font']."'");
        }
    }
    if (isset($configuration['theme_dash_global_font']) && is_string($configuration['theme_dash_global_font'])) {
        $registry->setKeyValue('theme_dash_global_font', $configuration['theme_dash_global_font']);
        // Global font is custom
        if($configuration['theme_dash_global_font'] == 'theme_dash_CustomGlobalFont') {
            $registry->setKeyValue('theme_dash_UsedGlobalFontUrl', $configuration['customGlobalFontUrl']);
            $registry->setKeyValue('theme_dash_UsedGlobalFontName', '"'.$configuration['customGlobalFontName'].'"');
        }
        // Global font is predefined
        else{
            $registry->setKeyValue('theme_dash_UsedGlobalFontUrl', "''");
            $registry->setKeyValue('theme_dash_UsedGlobalFontName', "'".$configuration['theme_dash_global_font']."'");
        }
    }
    if (isset($configuration['customTitleFontUrl']) && is_string($configuration['customTitleFontUrl'])) {
        $registry->setKeyValue('customTitleFontUrl', $configuration['customTitleFontUrl']);
    }
    if (isset($configuration['customTitleFontName']) && is_string($configuration['customTitleFontName'])) {
        $registry->setKeyValue('customTitleFontName', $configuration['customTitleFontName']);
    }
    if (isset($configuration['customGlobalFontUrl']) && is_string($configuration['customGlobalFontUrl'])) {
        $registry->setKeyValue('customGlobalFontUrl', $configuration['customGlobalFontUrl']);
    }
    if (isset($configuration['customGlobalFontName']) && is_string($configuration['customGlobalFontName'])) {
        $registry->setKeyValue('customGlobalFontName', $configuration['customGlobalFontName']);
    }

    // **** COLORS

    $registry->setKeyValue('headerBackgroundColor', '#' . $configuration['headerBackgroundColor']);
    $registry->setKeyValue('mainColor', '#' . $configuration['mainColor']);

    $registry->setKeyValue('sidebarTheme', $configuration['sidebarTheme']);

    $registry->setKeyValue('customSidebar_mainFontColor', '#' . $configuration['customSidebar_mainFontColor']);
    $registry->setKeyValue('customSidebar_secondFontColor', '#' . $configuration['customSidebar_secondFontColor']);
    $registry->setKeyValue('customSidebar_firstBackground', '#' . $configuration['customSidebar_firstBackground']);
    $registry->setKeyValue('customSidebar_secondBackground', '#' . $configuration['customSidebar_secondBackground']);
    $registry->setKeyValue('customSidebar_thirdBackground', '#' . $configuration['customSidebar_thirdBackground']);

    $registry->setKeyValue('sidebarFixed', $configuration['sidebarFixed']);

    $registry->setKeyValue('sidebarSearchBar', $configuration['sidebarSearchBar']);

    $registry->setKeyValue('sidebarOpenedDefault', $configuration['sidebarOpenedDefault']);

    $addon = bab_getAddonInfosInstance('theme_dash');

    // **** IMAGES

    $registry->setKeyValue('faviconImage', theme_dash_getImageUrl('favicon', 'images/' . $addon->getRelativePath()));

    $registry->setKeyValue('bannerImage', theme_dash_getImageUrl('banner'));

    $registry->setKeyValue('logoImage', theme_dash_getImageUrl('logo'));

    $registry->setKeyValue('sidebarImage', theme_dash_getImageUrl('sidebar'));

    $registry->setKeyValue('loginImage', theme_dash_getImageUrl('login'));

//     /* @var $Less Func_Less */
//     $Less = bab_functionality::get('less');

//     $compiledCssPath = new bab_Path(theme_dash_getCompiledCssPath());
//     foreach ($compiledCssPath as $file) {
//         if (!$file->isDir()) {
//             $file->delete();
//         }
//     }
//     try {
//         $Less->removeCompiledFiles();
//     } catch(bab_FileAccessRightsException $e) {
//         var_dump($e->getCode());
//         var_dump($e);
//         die;
//         bab_debug($e->getMessage());
//     }

}



// Exécution

if (!bab_isUserAdministrator()) {
    $babBody->addError(theme_dash_translate('Access denied.'));
    return;
}

$idx = bab_rp('idx', 'edit');


if (is_array($idx)) {
    list($idx,) = each($idx);
}


$msg = bab_rp('msg', null);
$errmsg = bab_rp('errmsg', null);
if (isset($errmsg)) {
    $babBody->addError($errmsg);
}
if (isset($msg)) {
    $babBody->addMessage($msg);
}

switch ($idx) {

    case 'save':
        $addon = bab_getAddonInfosInstance('theme_dash');
        $configuration = bab_rp('configuration', array());
        theme_dash_saveConfiguration($configuration);
        theme_dash_redirect($addon->getUrl().'configuration&idx=edit&msg='.urlencode(theme_dash_translate('Configuration saved')));
        break;

    case 'edit':
    default:
        theme_dash_editConfiguration();
        break;
}
