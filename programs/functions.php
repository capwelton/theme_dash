<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';





/**
 * @param string $str
 *
 * @return string
 */
function theme_dash_translate($str)
{
    return bab_translate($str, 'theme_dash');
}




function theme_dash_redirect($url, $message = null)
{
	global $babBody;

	if (null === $url) {
		die('<script type="text/javascript">history.back();</script>');
	}

	header('Location: ' . $url);
	die;
}


/**
 *
 * @param string        $text
 * @param Widget_Widget $widget
 */
function theme_dash_LabelledWidget($text, $widget)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-15em');
    $label->setAssociatedWidget($widget);
    $label->colon();

    $labelledWidget =  $W->FlowItems($label, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');

    return $labelledWidget;
}



/**
 *
 * @param string		$text
 * @param Widget_Widget	$widget
 * @param string		$name
 * @param bool			$checked
 * @return Widget_Displayable_Interface
 */
function theme_dash_OptionalLabelledWidget($text, $widget, $name, $checked)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-10em');
    $label->setAssociatedWidget($widget);
    $label->colon();

    $checkbox = $W->CheckBox();
    $checkbox->setAssociatedDisplayable($widget);
    $checkbox->setName($name);
    $checkbox->setUncheckedValue('0');
    $checkbox->setCheckedValue($widget->getId());
    if ($checked) {
        $checkbox->setValue($widget->getId());
    }
    $checkbox->setSizePolicy('widget-5em');
    $labelledWidget = $W->FlowItems($label, $checkbox, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');

    return $labelledWidget;
}





function theme_dash_shorten_values(&$defaultValues, $reg)
{
	$values = array();

	foreach ($defaultValues as $key => $value){
		$values[$key] = $reg->getValue($key, $value);
	}

	return $values;
}


/**
 * The folder path where the compiled css file will be stored.
 * @return string
 */
function theme_dash_getCompiledCssPath()
{
    $rootPath = realpath('.');
    $addon = bab_getAddonInfosInstance('theme_dash');
    $compiledCssPath = $rootPath . '/images/' . $addon->getRelativePath();

    return $compiledCssPath;
}


/**
 * The base url to access the compliled css files.
 * @return string
 */
function theme_dash_getCompiledCssBaseUrl()
{
    $addon = bab_getAddonInfosInstance('theme_dash');
    return  'images/' . $addon->getRelativePath();
}


/**
 *
 * @param string $configuration
 * @return array
 */
function theme_dash_getLessVariables($configuration = 'global')
{
	$defaultValues = array(
        'headerBackgroundColor' => '#26AADA',
		'mainColor'				=> '#DDDDDD',

        'bannerImage'           => "''",
        'logoImage'             => "''",
		'faviconImage'          => "''",
	    'sidebarImage'          => "''",
	    'loginImage'            => "''",

        'maxWidth'              => '0',
	    'headerHeight'          => '0',

	    'sidebarTheme'          => 'abyss',
	    'customSidebar_mainFontColor'          => '#E5E5E5',
	    'customSidebar_secondFontColor'        => '#FAFAFA',
	    'customSidebar_firstBackground'        => '#283C51',
	    'customSidebar_secondBackground'       => '#17232F',
	    'customSidebar_thirdBackground'        => '#427C97',

	    'sidebarFixed'          => 'sidebar-scroll-default',
	    'sidebarSearchBar'      => true,
	    'sidebarOpenedDefault'  => false,

	    'theme_dash_UsedTitleFontName'  => "''",
	    'theme_dash_UsedGlobalFontName'   => "''"
	);

	$values = array();

	foreach ($defaultValues as $key => $value) {
	    $values[$key] = bab_Registry::get('/theme_dash/' . $configuration . '/' . $key, $value);
	}

	return $values;
}


